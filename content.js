// content.js

const icons = document.querySelectorAll("div > g-img > img");

const sunny = chrome.runtime.getURL('images/sunny.png');
const cloudy = chrome.runtime.getURL('images/cloudy.png');

icons.forEach(element => {

    //Si on est sur une image de nuage + soleil -> cloudy
    const nuageux = ["Temps clair avec quelques nuages", "Partiellement couvert", "Nuageux dans l'ensemble", "Ensoleillé dans l'ensemble", "Partiellement ensoleillé"]
    if(nuageux.includes(element.getAttribute("alt"))){
       element.setAttribute("src", cloudy)
    }

    //si alt="Temps clair" -> suny
    if(element.getAttribute("alt") == "Temps clair"){
        element.setAttribute("src", sunny)
    }

});


